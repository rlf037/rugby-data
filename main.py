# pylint: disable=E1120

"""
Main script that the Streamlit server runs.

Import css fom style.css
Import DataFrame colour coding from teams.py
"""

import subprocess
import streamlit as st
import pandas as pd
import numpy as np

from git import Repo
from teams import styles
from backports.zoneinfo import ZoneInfo
from datetime import datetime

SECRET = 'raelene'


def get_update():
    """
    Returns the time the last commit was made.
    """
    repo = Repo('')
    headcommit = repo.head.commit
    time = datetime.fromtimestamp(headcommit.committed_date, tz=ZoneInfo(key='Australia/Sydney'))
    time = time.strftime("%a, %d %b %-I:%M %p")
    return f"<font size='2'>Last updated: {time} [AEST]</font>"


@st.cache
def get_css():
    """
    Returns the css for markdown.
    """
    with open('style.css') as style:
        css = style.read()
    return f'<style>{css}</style>'

@st.cache
def calc_results(df):
    """
    Function to caclulate the MAE of each prediction.
    """
    home_list, away_list, margin_list = [], [], []

    prediction = df['Pred'].values
    result = df['Result'].values

    for pred, true in zip(prediction, result):

        h_pred, a_pred = pred.split('-')
        h_true, a_true = true.split('-')

        home_list.append(abs(int(h_pred) - int(h_true)))
        away_list.append(abs(int(a_pred) - int(a_true)))

        m_pred = int(h_pred) - int(a_pred)
        m_true = int(h_true) - int(a_true)
        margin_list.append(abs(m_pred - m_true))

    df.insert(7, 'HΔ', home_list)
    df.insert(8, 'AΔ', away_list)
    df.insert(9, 'MΔ', margin_list)

    return df

def main():
    """
    Main function for streamlit to use.
    """
    st.markdown(get_css(), unsafe_allow_html=True)

    data = pd.read_csv('data.csv')
    predictions = pd.read_csv('locked.csv')
    predictions = predictions.drop(['Result'], axis=1)

    predicted = predictions.shape[0]
    completed = np.sum(data['Result'] != '-')
    matches = data.shape[0]

    completed_df = data.head(completed)
    upcoming_df = data.tail(matches - completed)
    upcoming_df = upcoming_df.drop(['Result'], axis=1)

    completed_df = calc_results(completed_df)

    st.title('Rugby Data')
    st.subheader('Completed Matches')
    completed_table = st.empty()
    st.subheader('Upcoming Matches')
    upcoming_table = st.empty()

    completed_table.dataframe(completed_df.iloc[::-1].style.applymap(styles), height=250)
    upcoming_table.dataframe(upcoming_df.style.applymap(styles), height=350, width=500)

    st.markdown(
        '<font size="2">Playing conditions (weather) not always factored into score predictions.</font>',
        unsafe_allow_html=True)

    password = st.text_input('', value='', max_chars=10, type='password')

    if st.button('Unlock'):
        if password.lower() == SECRET:
            st.success('Success')
            no_predictions = upcoming_df.iloc[predicted:]
            unlocked_df = predictions.append(no_predictions, ignore_index=True)
            upcoming_table.dataframe(unlocked_df.style.applymap(styles), height=350, width=500)
        elif password == 'update':
            git = subprocess.check_output('git pull', shell=True)
            st.success(git)
        elif password == 'cat':
            with open('inputs.txt', 'r') as inputs: txt = inputs.read()
            txt
        else:
            st.error('Wrong Password')
            with open('inputs.txt', 'r') as inputs:
                txt = inputs.read()
            txt += password
            with open('inputs.txt', 'w') as inputs:
                inputs.write(txt + '\n')

    st.write('')
    st.markdown(get_update(), unsafe_allow_html=True)


if __name__ == '__main__':
    st.beta_set_page_config(page_title='Rugby Data', page_icon='🏉', layout='centered', initial_sidebar_state='expanded')
    main()
